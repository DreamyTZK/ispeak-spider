const fs = require('fs/promises')
const _ = require('lodash')
const giteeFriend = require('./components/gitee')
const githubFriend = require('./components/github')
const { pageNumber } = require('./config')
// 将issue列表进行分组
const getIssueList = async () => {
  const allIssueList = [
    ...(await giteeFriend.getData()),
    ...(await githubFriend.getData())
  ]
  // 筛选排序
  allIssueList.sort(function (a, b) {
    return b.created_at < a.created_at ? -1 : 1
  })
  const chunkList = _.chunk(allIssueList, pageNumber)
  return chunkList
}
const index = async () => {
  const issueList = await getIssueList()
  const date = Date.now() + ''
  // 判断dist文件夹是否存在，不存在就创建
  try {
    await fs.access('dist')
  } catch (e) {
    await fs.mkdir('dist')
  }
  issueList.forEach(async (issue, index) => {
    // 当前页码
    const page = index + 1
    const obj = {
      data: issue,
      next: page == issueList.length ? null : page + 1 + '.json',
      page: issueList.length,
      date
    }
    const friendStr = JSON.stringify(obj)
    await fs.writeFile(`./dist/${page}.json`, friendStr, 'utf8', (err) => {
      if (err) throw err
    })
  })
  const pageageFile = {
    name: process.env.NPM_NAME || '',
    version: `${date[0]}.${date[1]}.${date.slice(2, 13)}`,
    main: 'index.js',
    description: '一个存储爬取到的issue数据的仓库。'
  }
  const pageageFileStr = JSON.stringify(pageageFile)
  await fs.writeFile('./dist/package.json', pageageFileStr, 'utf8', (err) => {
    if (err) throw err
  })
  console.log('写入完成')
}
index()
